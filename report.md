
# Introduction

Driver Assistance systems are systems developed to automate/adapt/enhance vehicle systems for safety and better driving experience.

# Approach

## Optical Flow

Optical flow helps us understand the relative direction of vehicles. We would be able to understand how the vehicles are moving. This would also help us in calculation of velocity of our vehicle.

### ![img](//www.cvlibs.net/projects/objectsceneflow/showcase.jpg)

## Traffic Signs Detection

Traffic Sign detection help us detect speed limits and other traffic signs like T-junction, one-way help us judge understand how the driver should drive. It also helps us understand the speed limits.

### ![img](//www.cs.bris.ac.uk/home/greenhal/road_sign_example_2.jpg)

## Lane Detection

Lane Detection in Indian context can be challenging, since a lot of roads are not marked properly. So, the approach for this is to mark the road ends instead of the lanes directly and split the width of the road into two or more parts depending on the width.

### ![img](//cvrr.ucsd.edu/LISA/images/results3.JPG)

## Fog Detection

We use fog detection to recognize dangerous weather conditions to drive in presence of fog. We will compute fog density and compute the visibility distance. We compare this to velocity of the driver to see if he is driving within an acceptable speed range.

### ![img](//blog.toyotaoforlando.com/wp-content/uploads/safe-driving-tips1.jpg)
